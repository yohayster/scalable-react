/*
 *
 * NavigationContainer constants
 *
 */

export const REQUEST_TOPICS = 'app/NavigationContainer/REQUEST_TOPCIS';
export const REQUEST_TOPICS_SUCCEEDED = 'app/NavigationContainer/REQUEST_SUCCEEDED';
export const REQUEST_TOPICS_FAILED = 'app/NavigationContainer/REQUEST_TOPICS_FAILED';
export const SELECT_TOPIC = 'app/NavigationContainer/SELECT_TOPIC';